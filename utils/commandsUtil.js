const Discord = require('discord.js');
const fs = require('fs');
module.exports = {
	getSubCommands(commandName) {
		const subCmds = new Discord.Collection();
		const cmdFiles = fs.readdirSync(`./commands/${commandName}`).filter(file => file.endsWith('.js'));
		for (const file of cmdFiles) {
			const sousCmd = require(`../commands/${commandName}/${file}`);
			subCmds.set(sousCmd.name, sousCmd);
		}
		return subCmds;
	},
};
