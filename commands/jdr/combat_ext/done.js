const combat = require('../classes/classeCombat.js');

module.exports = {
	name: 'done',
	description: 'Mets fin au tour du joueur courant.',
	alias: ['fin', 'jePlaceUneCarteFaceCachéEtJeTerminesMonTour'],
	tmp: true,
	execute(msg, args) {
		try{
			const monCombat = combat.fromJSON();
			monCombat.tourSuivant(msg);
			monCombat.stockerJSON();
		}
		catch (err) {
			console.error(err);
		}
	},
};