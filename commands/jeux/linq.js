module.exports = {
	name: 'linq',
	description: 'Lance une partie de linq',
	serverOnly: true,
	repertory: 'commands/jeux',
	args: true,
	usage: 'Joueur1 ... Joueur4 [Joueur 5 ... Joueur 8]',
	execute(message, args) {

		const fs = require('fs');
		var players = []; // tableau contenant les joueurs

		//On ajoute les personnes mentionnées au tableau de sjoueurs
	  message.mentions.members.forEach(function(guildMember, guildMemberId) {
	    players.push(guildMember);
	  })

		//On vérifie qu'il y a assez de joueurs pour lancer la partie
		if(players.length < 4) {
			message.channel.send("Trop peu de joueurs");
			return;
		}
		else if(players.length > 8) {
			message.channel.send("Trop de joueurs");
			return;
		}

		//On lance l'initialisation de la partie
		else {

			//On récupère les mots de passe
			try {
        var data = fs.readFileSync('./datas/jeux/linq/noms.txt', 'utf8');
        var words = data.split("\n");
        for (mot in words) {
          words[mot] = words[mot].replace("\r", '');
        };
      } catch(e) {
        console.log('Error:', e.stack);
      }

			//les deux mots de passe
      let mot1 = words[Math.floor(Math.random() * words.length)];
  		let mot2 = words[Math.floor(Math.random() * words.length)];

      /******************Deroulement de la partie*************************/

      var roles = [];

      for (let i = 0; i < players.length; i++) {
        roles.push(i);
      }

      /**********************Partie +6 joueurs************************/

      if (players.length > 6) {

        for (name in players) {

          player = players[name];
          let role = Math.floor(Math.random() * roles.length); /***********0, 1, 2, 3 => espion************/

          if (roles[role] == 0 || roles[role] == 1) {
            message.client.fetchUser(player)
              .then(user => {user.send("Tu es un espion et ton mot de passe est: "+mot1)})
          }
          else if (roles[role] == 2 || roles[role] == 3) {
            message.client.fetchUser(player)
              .then(user => {user.send("Tu es un espion et ton mot de passe est: "+mot2)})
          }
          else {
            message.client.fetchUser(player)
              .then(user => {user.send("Tu es un contre-espion")})
          }
          roles.splice(role, 1);
        }
      }

      /**********************Partie 6 joueurs**************************/

      else {

        for (name in players) {

          player = players[name];
          let role = Math.floor(Math.random() * roles.length); /***********0, 1=> espion************/

          if (roles[role] == 0 || roles[role] == 1) {
            message.client.fetchUser(player)
              .then(user => {user.send("Tu es un espion et ton mot de passe est: "+mot1)})
          }

          else {
            message.client.fetchUser(player)
              .then(user => {user.send("Tu es un contre-espion")})
          }
          roles.splice(role, 1);
        }

      }
		}
	},
};
