const Discord = require('discord.js');
const Server = require('../../datas/classesXp/server.js');
const User = require('../../datas/classesXp/user.js');
const Vol = require('../../datas/classesXp/vol.js');

module.exports = {
  name: 'vu',
  alias: ['connard'],
  description: 'Prend le voleur sur le fait et redonne l\'xp qu\'il a volé',
  serverOnly: true,
  cooldown: 0.1,
  execute(message, args){
    // On vérifie que le serveur dans lequel il se trouve accepte l'xp et le vol
    var server = Server.getServer(message.guild.id);
    if (server == null) return message.channel.send('`❌ Ce serveur n\'a pas encore été configuré pour l\'utilisation de l\'xp.' +
                                                    '\nPour configurer le serveur écrivez: ' + prefix + 'setup`');
    if (!server.acceptXp) return message.channel.send('`❌ Ce serveur n\'accepte pas l\'utilisation de l\'xp, donc tu ne peux pas être volé !`');
    if (!server.acceptThief) return message.channel.send('`❌ Ce serveur n\' accepte pas l\'utilisation de la fonction vol, donc personne n\'a pu te voler !`');

    var listVu = Vol.checkVols(message.member);
    if (listVu.length == 0) return message.channel.send("`Tu n'as pas encore été volé, ou alors il est déjà trop tard...`");
    var res = {};
    listVu.forEach(function(vol){
      // On récupère l'user du voleur
      var user = User.getUser(vol.voleur.user.id);
      if (vol.voleur.user.username in res) {
        var amount = Math.floor(Math.random() * 25 * ((user.getThiefLevel()+1)/10) + 1);
        user.addThiefXp(amount * (-1));
        res[vol.voleur.user.username].xp += amount;
        res[vol.voleur.user.username].fois++;
      }
      else {
        var amount = Math.floor(Math.random() * 25 * ((user.getThiefLevel()+1)/10) + 1);
        user.addThiefXp(amount * (-1));
        res[vol.voleur.user.username] = {xp: amount, fois: 1};

      }
    });

    var text = "```md\n# Voleurs pris sur le fait par " + message.author.username + ":\n";
    for (var voleur in res){
      text += "* " + voleur + ": < -" + res[voleur].xp + "xp de voleur > (vu " + res[voleur].fois + " fois)\n";
    };
    Vol.clearVols(message.member);
    return message.channel.send(text + "```");
  }
}
