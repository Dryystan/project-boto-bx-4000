module.exports = {
	name: 'timer',
	alias: ['timer', 'compte'],
	description: 'Actualise les fonctions voulus.',
	execute(msg, args) {
		if (!isNaN(args[0]) && parseInt(args[0]) > 0) {
			let monTimer;
			msg.channel.send('Début du timer !').then(message => monTimer = message);
			let temps = 1;
			while (temps < parseInt(args[0]) + 1) {
				(function(temps) {
					setTimeout(function() {
						monTimer.edit('Temps écoulé : ' + temps + ' sec.');
					}, 1000 * temps);
				})(temps++);
			}
		}else{
			msg.channel.send('Erreur : ' + args[0] + ' n\'est pas un temps valide');
		}
	},
};