const Discord = require('Discord.js');
module.exports = {
	name: 'avatar',
	alias: ['ava', 'profilPic', 'pp'],
	description: 'Montre l\'avatar de l\'utilisateur mentionné!',
	serverOnly: true,
	usage: '[user]',
	execute(message, args) {
		const embd = new Discord.RichEmbed()
			.setColor('#AA11BB');
		if(message.mentions.users.array().length > 0) {
			message.mentions.users.array().forEach(user => {
				embd.setTitle(`L'avatar de ${user.username}`)
					.setImage(user.avatarURL);
				message.channel.sendEmbed(embd);
			});
		}
		else {
			embd.setTitle(`L'avatar de ${message.author.username}`)
				.setImage(message.author.avatarURL);
			message.channel.sendEmbed(embd);
		}

	},
};
